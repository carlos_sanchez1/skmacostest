import React from 'react';
import {View, Text, Dimensions} from 'react-native';

import LeftMenuBar from '../LeftMenuBar';
import Navbar from '../Navbar';

const Container = (props) => {
  console.log(Dimensions.get('window').height);
  return (
    <View style={{flexDirection: 'row', height: 694}}>
      <LeftMenuBar />
      <View>
        <Navbar />
        <Text>CONTAINER</Text>
        {props.children}
      </View>
    </View>
  );
};

export default Container;
