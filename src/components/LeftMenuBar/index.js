import React, {useEffect, useState} from 'react';
import {View, Text} from 'react-native';

import ButtonNav from './ButtonNav';
import Button from '../Button';

import Ionicons from 'react-native-vector-icons/Ionicons';
import Feather from 'react-native-vector-icons/Feather';

import {useNavigationState, useNavigation} from '@react-navigation/native';

const LeftMenuBar = () => {
  const navigation = useNavigation();
  const routes = useNavigationState((state) => state.routes);
  const currentRoute = routes[routes.length - 1].name;
  console.log('currentRoute: ', currentRoute);

  const [tasksScreenActive, setTasksScreenActive] = useState(false);
  const [routinesScreenActive, setRoutinesScreenActive] = useState(false);

  useEffect(() => {
    currentRoute === 'Tasks'
      ? setTasksScreenActive(true)
      : setTasksScreenActive(false);

    currentRoute === 'Routines'
      ? setRoutinesScreenActive(true)
      : setRoutinesScreenActive(false);
  }, [currentRoute]);

  console.log('tasks', tasksScreenActive);
  console.log('routines', routinesScreenActive);
  return (
    <View
      style={{
        backgroundColor: 'lightblue',
        width: 180,
        paddingVertical: 40,
        alignItems: 'center',
        justifyContent: 'space-between',
      }}>
      <View
        style={{
          backgroundColor: 'lightgray',
          alignItems: 'center',
          justifyContent: 'space-between',
        }}>
        {/* <Text>LeftMenuBar</Text> */}
        <Ionicons
          name="school"
          size={50}
          color="white"
          style={{marginBottom: 30}}
        />
        <View>
          <ButtonNav
            textButton="Tasks"
            goScreen={() => {
              navigation.navigate('Tasks');
            }}
            styleBtn={{
              backgroundColor: tasksScreenActive ? 'red' : 'gray',
            }}
            active={tasksScreenActive}
          />
          <ButtonNav
            textButton="Routines"
            goScreen={() => {
              navigation.navigate('Routines');
            }}
            styleBtn={{
              backgroundColor: routinesScreenActive ? 'red' : 'gray',
            }}
            active={routinesScreenActive}
          />
        </View>
      </View>
      <Ionicons name="settings-sharp" size={35} color="white" />
    </View>
  );
};

export default LeftMenuBar;
