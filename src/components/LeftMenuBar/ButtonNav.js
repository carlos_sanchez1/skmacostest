import React from 'react';
import {View, Text} from 'react-native';

import Button from '../Button';

const ButtonNav = ({textButton, goScreen, active}) => {
  return (
    <Button
      content={
        <View>
          <Text>{textButton}</Text>
        </View>
      }
      onPress={goScreen}
      styleBtn={{
        backgroundColor: active ? 'red' : 'lightgray',
        width: 170,
        paddingLeft: 20,
        paddingVertical: 10,
        marginBottom: 10,
        borderRadius: 9,
      }}
    />
  );
};

export default ButtonNav;
