import React from 'react';
import {View, Text, Dimensions} from 'react-native';

const Navbar = () => {
  console.log(Dimensions.get('screen').width);

  let navWidth = Dimensions.get('screen').width - 180;

  return (
    <View style={{backgroundColor: 'lightgreen', width: navWidth}}>
      <Text>NAVBAR</Text>
    </View>
  );
};

export default Navbar;
