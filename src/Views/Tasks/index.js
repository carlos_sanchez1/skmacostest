import React from 'react';
import {View, Text} from 'react-native';

import Container from '../../components/Container';

const Tasks = () => {
  return (
    <Container>
      <View>
        <Text>Tasks page</Text>
      </View>
    </Container>
  );
};

export default Tasks;
